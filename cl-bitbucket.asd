#|
  This file is a part of cl-bitbucket project.
   Copyright (c) 2012 Pocket7878 (poketo7878@gmail.com)
|#

#|
  cl-bitbucket - Common Lisp Bitbucket API wrapper.

  Author: Pocket7878 (poketo7878@gmail.com)
|#

(in-package :cl-user)
(defpackage cl-bitbucket-asd
  (:use :cl :asdf))
(in-package :cl-bitbucket-asd)

(defsystem cl-bitbucket
    :version "0.1-SNAPSHOT"
    :author  "Pocket7878"
    :license  "GPLv3"
    :depends-on (:cl-annot :drakma :cl-json)
    :components ((:module "src"
			  :components
			  ((:file "package")
			   (:file "setup" :depends-on ("utils"))
			   (:file "utils")
			   (:file "user" :depends-on ("setup"))
			   (:file "email" :depends-on ("setup"))
			   (:file "changeset" :depends-on ("setup"))
			   (:file "events" :depends-on ("setup")))))
    :description "CommonLisp Bitbucket API wrapper")

(defmethod asdf:perform :after ((op load-op) (c (eql (find-system :cl-bitbucket))))
  (pushnew :cl-bitbucket *features*))

	       
#|
  This file is a part of cl-bitbucket project.
   Copyright (c) 2012 Pocket7878 (poketo7878@gmail.com)
|#

(in-package :cl-bitbucket)

(cl-annot:enable-annot-syntax)

@export
(defun events (username &key (start 0) (limit 25))
  (decode-json-from-string
   (http-request
    (create-get-uri
     (concatenate 'string *api-base-uri* "/users/" username "/events/")
     `(("start" . ,start) ("limit" . ,limit))))))

@export
(defun repo-events (username repo-slug &key (start 0) (limit 25) (type "" type-specifiedp))
  (decode-json-from-string
   (http-request
    (create-get-uri
     (concatenate 'string *api-base-uri* "/repositories/" username "/" repo-slug "/events/")
     `(("start" . ,start) ("limit" . ,limit) ,(when type-specifiedp '("type" . type)))))))
#|
  This file is a part of cl-bitbucket project.
   Copyright (c) 2012 Pocket7878 (poketo7878@gmail.com)
|#

(in-package :cl-bitbucket)

(cl-annot:enable-annot-syntax)

@export
(defun create-get-uri (base-uri parameters)
  (concatenate 'string
	       base-uri "?"
	       (format nil "~{~a~^&~}"
		       (loop for param in parameters
			  collect
			    (concatenate 'string (car param) "=" (write-to-string (cdr param)))))))


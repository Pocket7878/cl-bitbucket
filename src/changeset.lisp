#|
  This file is a part of cl-bitbucket project.
   Copyright (c) 2012 Pocket7878 (poketo7878@gmail.com)
|#

(in-package :cl-user)
(in-package :cl-bitbucket)

(cl-annot:enable-annot-syntax)

@export
(defun changeset (username repo-slug &optional changeset &key (diffstat nil))
  (let ((base-uri (concatenate 'string
			       *api-base-uri* "/repositories/" username
			       "/" repo-slug "/changesets/" changeset)))
    (decode-json-from-string
     (http-request
      (if diffstat
	  (concatenate 'string base-uri "/diffstat")
	  base-uri)
      ))))
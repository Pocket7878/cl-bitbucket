#|
  This file is a part of cl-bitbucket project.
   Copyright (c) 2012 Pocket7878 (poketo7878@gmail.com)
|#

(in-package :cl-bitbucket)

(cl-annot:enable-annot-syntax)

@export
(defun user (username &key (password "" password-suppliedp))
  (decode-json-from-string
   (http-request
    (concatenate 'string
		 *api-base-uri*
		 "/users/" username)
    :basic-authorization (when password-suppliedp (list username password)))))

@export
(defun create-user (username email password)
  (decode-json-from-string
   (http-request
    (concatenate 'string *api-base-uri* "/newuser/")
    :method :post
    :parameters
    `(("email" . ,email) ("password" . ,password) ("username" . ,username)))))

#|
  This file is a part of cl-bitbucket project.
   Copyright (c) 2012 Pocket7878 (poketo7878@gmail.com)
|#

(in-package :cl-bitbucket)

(cl-annot:enable-annot-syntax)

@export
(defun email (username password &optional email) 
  (decode-json-from-string
   (http-request
    (concatenate 'string *api-base-uri* "/emails/" email)
    :basic-authorization (list username password))))

@export
(defun add-email (username password new-email) 
  (decode-json-from-string
   (http-request
    (concatenate 'string *api-base-uri* "/emails/" new-email)
    :method :post
    :basic-authorization (list username password))))

@export
(defun set-email-as-primary (username password email)
  (decode-json-from-string
   (http-request
    (concatenate 'string *api-base-uri* "/emails/" email)
    :method :post
    :basic-authorization (list username password)
    :parameters `(("primary" . "true")))))

@export
(defun remove-email (username password email)
  (decode-json-from-string
   (http-request
    (concatenate 'string *api-base-uri* "/emails/" email)
    :method :delete
    :basic-authorization (list username password))))

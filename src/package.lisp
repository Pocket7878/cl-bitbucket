#|
  This file is a part of cl-bitbucket project.
   Copyright (c) 2012 Pocket7878 (poketo7878@gmail.com)
|#

;; Define just package
(in-package :cl-user)
(defpackage cl-bitbucket
  (:use :cl)
  (:import-from :drakma :http-request)
  (:import-from :json :decode-json-from-string))








#|
  This file is a part of cl-bitbucket project.
   Copyright (c) 2012 Pocket7878 (poketo7878@gmail.com)
|#

(in-package :cl-bitbucket)

(cl-annot:enable-annot-syntax)

(setf *drakma-default-external-format* :utf-8)
(pushnew '("application" . "json") drakma:*text-content-types* :test #'equal)

(defvar *api-base-uri* "https://api.bitbucket.org/1.0")
